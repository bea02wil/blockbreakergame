﻿Ссылка на YouTube видео.
--------------------------------------------------------------------------------------------------------------------------------------
Кликните по изображению (с зажатым CTRL, чтобы открыть в новом окне) для перехода. Если ссылка не работает, воспользуйтесь прямой ниже.

[![AnimationAndScrollView](https://bitbucket.org/bea02wil/blockbreakergame/raw/master/BlockBreaker.png)](https://youtu.be/fPHbfBrKREE "Go to the video")

https://youtu.be/fPHbfBrKREE