﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigBallIcon : MonoBehaviour
{
    ballLaserIcon iconBallLaser;
    float iconTimer;
    bool checkIconBallLaserObject;

    private void Start()
    {
        iconBallLaser = FindObjectOfType<ballLaserIcon>();
    }

    private void Update()
    {
        DestoyIfTimerMore5();
        ChangeIBlaserPosIfPointsBuzy();
    }

    private void ChangeIBlaserPosIfPointsBuzy()
    {
        if (checkIconBallLaserObject == false && iconBallLaser != null)
        {
            if (transform.position == iconBallLaser.transform.position)
            {
                iconBallLaser.transform.position = new Vector2(iconBallLaser.transform.position.x - 0.5f, 6.14f);
                checkIconBallLaserObject = true;
            }
        }
    }

    private void DestoyIfTimerMore5()
    {
        iconTimer += Time.deltaTime;
        if (iconTimer > 5)
            Destroy(gameObject);
    }
}
