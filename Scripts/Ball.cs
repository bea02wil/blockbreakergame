﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] float xPos = 0f;
    [SerializeField] float yPos = 15f;
    [SerializeField] float randomPlusToSpeed = 0.2f;
    [SerializeField] GameObject playOnBallAlways;

    Pedal pedal;
    Pause gameOn;
    Vector2 posDifference;
    public Rigidbody2D BallStartSpeed { get; set; }
    GameObject ballSparkles;
    public SpriteRenderer BallColor { get; set; }
    ParticleSystem ballSparklesColor;     
    bool ballNotOnPaddle;
    bool checkLeftButton = true;

    public bool BallNotOnPaddle
    {
        get { return ballNotOnPaddle; }
        set { ballNotOnPaddle = value; }
    }

    public bool CheckLeftButton
    {
        get { return checkLeftButton; }
        set { checkLeftButton = value; }
    }
    
    public GameObject PlayOnBallAlways
    {
        get { return playOnBallAlways; }
        set { playOnBallAlways = value; }
    }

    void Start()
    {
        StartSettings();
    }

    private void StartSettings()
    {
        pedal = FindObjectOfType<Pedal>();
        gameOn = FindObjectOfType<Pause>();
        posDifference = transform.position - pedal.transform.position;
        BallStartSpeed = GetComponent<Rigidbody2D>();
        BallColor = GetComponent<SpriteRenderer>();
        ballSparklesColor = playOnBallAlways.GetComponent<ParticleSystem>();
        ballSparklesColor.startColor = new Color32(88, 247, 255, 255);
    }

    void Update()
    {
        LockToPaddle();
        LaunchBall();
        SparklesEffect();
        ChangeBallColor();
    }

    public void LockToPaddle()
    {
        Vector2 ballPos = (Vector2)pedal.transform.position + posDifference;
        if (BallNotOnPaddle == false)      
            transform.position = ballPos;                                                  
    }

    private void LaunchBall()
    {
        if (CheckLeftButton == true)
        {
            if (Input.GetMouseButton(0))
            {
                if (gameOn.Paused)                
                    BallNotOnPaddle = false;
                
                else
                {
                    BallNotOnPaddle = true;
                    pedal.DontPlayBallSoundAtStart = true;
                    BallStartSpeed.velocity = new Vector2(xPos, yPos);
                    CheckLeftButton = false;
                    if (transform.localScale.x == 1.5f && transform.localScale.y == 1.5f)
                        BallStartSpeed.velocity = new Vector2(0, 16);
                }             
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        SpeedUp();
    }

    private void SpeedUp()
    {
        float randomSpeed = Random.Range(0.2f, randomPlusToSpeed);
        float xPlus = randomSpeed;
        float yPlus = randomSpeed;
        Vector2 randomSpeedCoordinates = new Vector2(xPlus, yPlus);    
        BallStartSpeed.velocity += randomSpeedCoordinates;
        if (transform.position.y > 11.4)
            BallStartSpeed.velocity -= randomSpeedCoordinates;
    }

    private void SparklesEffect()
    {
        ballSparkles = Instantiate(playOnBallAlways, transform.position, transform.rotation);
        Destroy(ballSparkles, 0.3f);
    }

    private void ChangeBallColor()
    {
        if (transform.localScale.x == 1 && transform.localScale.y == 1)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                BallColor.color = new Color32(255, 0, 10, 255);
                ballSparklesColor.startColor = new Color32(255, 0, 10, 255);
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                BallColor.color = new Color32(16, 255, 0, 255);
                ballSparklesColor.startColor = new Color32(16, 255, 0, 255);
            }
            else if (Input.GetKeyDown(KeyCode.D))
            {
                BallColor.color = new Color32(250, 255, 1, 255);
                ballSparklesColor.startColor = new Color32(250, 255, 1, 255);
            }
            else if (Input.GetKeyDown(KeyCode.F))
            {
                BallColor.color = new Color32(88, 247, 255, 255);
                ballSparklesColor.startColor = new Color32(88, 247, 255, 255);
            }
        }      
    } 
}
