﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BlocksCounter : MonoBehaviour
{
    [SerializeField] int breakableBlocks;

    BottomTrigger bottomTrigger;

    private void Start()
    {
        bottomTrigger = FindObjectOfType<BottomTrigger>();
    }

    public void CountBlocks()
    {
        breakableBlocks++;
    }  

    public void CheckCountOfBlocksAndLoadNextLvl()
    {
        breakableBlocks--;
        if (breakableBlocks == 0)
        {
            FindObjectOfType<SceneLoader>().LoadNextLevel();
            bottomTrigger.gameObject.SetActive(false);
        }
    }  
}
