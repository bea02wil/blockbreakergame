﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scores : MonoBehaviour
{
    [SerializeField] Text yourScore;
    [SerializeField] Text bestScore;

    GameStats gameStats;
    int bestScoreValue;

    private void Start()
    {
        gameStats = FindObjectOfType<GameStats>();
        yourScore.text = gameStats.CurrentScore.ToString();
        bestScoreValue = PlayerPrefs.GetInt("bestScoreValue", bestScoreValue);
        bestScore.text = bestScoreValue.ToString();
    }

    private void Update()
    {
        if (gameStats.CurrentScore > bestScoreValue)
        {
            bestScoreValue = gameStats.CurrentScore;
            bestScore.text = bestScoreValue.ToString();
            PlayerPrefs.SetInt("bestScoreValue", bestScoreValue);
        }
    }
}
