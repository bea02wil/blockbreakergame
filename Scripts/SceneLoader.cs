﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
   [SerializeField] AudioClip nextLevelSound;
   [SerializeField] AudioClip gameOverSound;

    Image fadeImage;
    bool fadeIn;
    bool fadeOut;
    float volume = 0.4f;
    float alphaColor;  
    public bool FadeInOtherLvls { get; set; }         
    public Color fadeInColor;
    public Color fadeOutColor;  

    private void Start()
    {
        fadeImage = FindObjectOfType<Image>();
        fadeImage.gameObject.SetActive(true);
        fadeImage.color = new Color(fadeOutColor.r, fadeOutColor.g, fadeOutColor.b, 1f);
        fadeOut = true;
    }

    private void Update()
    {
        FadeInFadeOut();        
    }
 
    private void FadeInFadeOut()
    {
        if (fadeIn)
        {           
            alphaColor = Mathf.Lerp(fadeImage.color.a, 1, Time.deltaTime * 2f);
            fadeImage.color = new Color(fadeInColor.r, fadeInColor.g, fadeInColor.b, alphaColor);
        }

        if (fadeOut)
        {
            alphaColor = Mathf.Lerp(fadeImage.color.a, 0, Time.deltaTime * 2f);
            fadeImage.color = new Color(fadeOutColor.r, fadeOutColor.g, fadeOutColor.b, alphaColor);
        }

        if (FadeInOtherLvls)
        {
            alphaColor = Mathf.Lerp(fadeImage.color.a, 1, Time.deltaTime * 2f);
            fadeImage.color = new Color(fadeInColor.r, fadeInColor.g, fadeInColor.b, alphaColor);
        }                

        if (alphaColor > 0.95 && fadeIn)
        {
            fadeIn = false;
            StartGame();
        }

        if (alphaColor > 0.95 && FadeInOtherLvls)
        {
            FadeInOtherLvls = false;
        }      
           
        if (alphaColor < 0.05 && fadeOut)
        {
            fadeOut = false;
            fadeImage.gameObject.SetActive(false);
        }
    }

   public void LoadNextLevel()
   {
       StartCoroutine(WaitAndLoad());
   }

   IEnumerator WaitAndLoad()
   {
       fadeImage.gameObject.SetActive(true);
       FadeInOtherLvls = true;
       AudioSource.PlayClipAtPoint(nextLevelSound, Camera.main.transform.position, volume);      
       yield return new WaitForSeconds(2);
       SceneManager.LoadScene(CurrentLevel() + 1);
   }

   private void StartGame()
   {
       SceneManager.LoadScene(1);
   }

   public void ButtonStartGame()
   {
        fadeImage.gameObject.SetActive(true);
        fadeImage.color = new Color(fadeInColor.r, fadeInColor.g, fadeInColor.b, 0);
        fadeIn = true;
   }

   public void GameOver()
   {
       StartCoroutine(LoadGameOver());      
   }
   
   IEnumerator LoadGameOver()
   {
        AudioSource.PlayClipAtPoint(gameOverSound, Camera.main.transform.position, volume);
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("GameOver");
   }

   public int CurrentLevel()
   {
       int currentLevel = SceneManager.GetActiveScene().buildIndex;
       return currentLevel;
   }
    
   private void StartRulesMenu()
   {
       SceneManager.LoadScene("RulesMenu");
       Cursor.visible = true;
   }    
   
   public void LoadStartMenuFromGameOver()
   {
       FindObjectOfType<GameStats>().ResetGameStats();
       SceneManager.LoadScene(0);
       Cursor.visible = true;
   }
}
