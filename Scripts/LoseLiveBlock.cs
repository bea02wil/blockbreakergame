﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseLiveBlock : MonoBehaviour
{
    [SerializeField] AudioClip blockDestroySound;
    [SerializeField] AudioClip blockMinusLiveSound;

    Ball ball;
    GameStats gameStats;
    BlocksCounter blockCounter;
    float volume = 0.5f;

    private void Start()
    {
        blockCounter = FindObjectOfType<BlocksCounter>();
        ball = FindObjectOfType<Ball>();
        gameStats = FindObjectOfType<GameStats>();       
        FindAndCountBlocks();
    }

    private void Update()
    {
        BigBallPower();
    }

    private void FindAndCountBlocks()
    {
        if (tag == "YellowBlock" ||
           tag == "GreenBlock" || tag == "RedBlock"
           || tag == "BlueBlock")
            FindObjectOfType<BlocksCounter>().CountBlocks();
    }

    private void BigBallPower()
    {
        if (ball.transform.localScale.x == 1.5f && ball.transform.localScale.y == 1.5f)
            GetComponent<Collider2D>().isTrigger = true;

        if (ball.transform.localScale.x == 1 && ball.transform.localScale.y == 1)
            GetComponent<Collider2D>().isTrigger = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (tag == "YellowBlock")
            YellowBlockLogic();

        if (tag == "GreenBlock")
            GreenBlockLogic();

        if (tag == "RedBlock")
            RedBlockLogic();

        if (tag == "BlueBlock")
            BlueBlockLogic();
    }

    private void BlueBlockLogic()
    {     
        if (ball.BallColor.color == new Color32(88, 247, 255, 255))
        {
            AudioSource.PlayClipAtPoint(blockDestroySound, Camera.main.transform.position, volume);
            gameStats.BonusToScore(20);
            Destroy(gameObject);
            blockCounter.CheckCountOfBlocksAndLoadNextLvl();
        }
        else
        {
            AudioSource.PlayClipAtPoint(blockMinusLiveSound, Camera.main.transform.position);
            gameStats.MinusLiveOnLoose();
        }
    }

    private void RedBlockLogic()
    {   
        if (ball.BallColor.color == new Color32(255, 0, 10, 255))
        {
            AudioSource.PlayClipAtPoint(blockDestroySound, Camera.main.transform.position, volume);
            gameStats.BonusToScore(20);
            Destroy(gameObject);
            blockCounter.CheckCountOfBlocksAndLoadNextLvl();
        }
        else
        {
            AudioSource.PlayClipAtPoint(blockMinusLiveSound, Camera.main.transform.position);
            gameStats.MinusLiveOnLoose();
        }
    }

    private void GreenBlockLogic()
    {     
        if (ball.BallColor.color == new Color32(16, 255, 0, 255))
        {
            AudioSource.PlayClipAtPoint(blockDestroySound, Camera.main.transform.position, volume);
            gameStats.BonusToScore(20);
            Destroy(gameObject);
            blockCounter.CheckCountOfBlocksAndLoadNextLvl();
        }
        else
        {
            AudioSource.PlayClipAtPoint(blockMinusLiveSound, Camera.main.transform.position);
            gameStats.MinusLiveOnLoose();
        }
    }

    private void YellowBlockLogic()
    {     
        if (ball.BallColor.color == new Color32(250, 255, 1, 255))
        {
            AudioSource.PlayClipAtPoint(blockDestroySound, Camera.main.transform.position, volume);
            gameStats.BonusToScore(20);
            Destroy(gameObject);
            blockCounter.CheckCountOfBlocksAndLoadNextLvl();
        }
        else
        {
            AudioSource.PlayClipAtPoint(blockMinusLiveSound, Camera.main.transform.position);
            gameStats.MinusLiveOnLoose();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ball")
        {
            AudioSource.PlayClipAtPoint(blockDestroySound, Camera.main.transform.position, volume);
            FindObjectOfType<GameStats>().BonusToScore(1);
            FindObjectOfType<BlocksCounter>().CheckCountOfBlocksAndLoadNextLvl();
            Destroy(gameObject);
        }
    }
}
