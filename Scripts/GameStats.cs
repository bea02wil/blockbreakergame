﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameStats : MonoBehaviour
{
    [SerializeField] Text pointsText;
    [SerializeField] Text livePoints;
    [SerializeField] Text currentLevelText;
    [SerializeField] int pointsPerBlock = 10;

    int currentScore;    
    public int LivesCounter { get; set; }  
    float livePointsTextTimer;
    bool levelCheck;
    bool allSoundMute = true;
   

    public int CurrentScore
    {
        get { return currentScore; }
        set { currentScore = value; }
    }

    public Text PointsText
    {
        get { return pointsText; }
        set { pointsText = value; }
    }

    public Text LivePoints
    {
        get { return livePoints; }
        set { livePoints = value; }
    }

    private void Awake()
    {
        int gameStatsCount = FindObjectsOfType<GameStats>().Length;
        if (gameStatsCount > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        
        else        
            DontDestroyOnLoad(gameObject);

       // DisableGameCanvas();
    }

    private void Start()
    {
        Cursor.visible = false;
        LivesCounter = 3;
        PointsText.text = CurrentScore.ToString();
        LivePoints.text = LivesCounter.ToString();    
    }

    private void Update()
    {
        FlashLivePointsText();
        GetNewLevel();
        DisableGameCanvas();
        VisibleCursorGameOverScene();
    }

    private void VisibleCursorGameOverScene()
    {
        if (CurrentLevel() == 7)
            Cursor.visible = true;
    }

    private void DisableGameCanvas()
    {
        if (CurrentLevel() == 7)
        {
            gameObject.transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    private void GetNewLevel()
    {
        currentLevelText.text = CurrentLevel().ToString();        
    }

    public int CurrentLevel()
    {
        int currentLevel = SceneManager.GetActiveScene().buildIndex;
        return currentLevel;
    }

    private void FlashLivePointsText()
    {
        if (LivesCounter == 0)
        {
            livePointsTextTimer += Time.deltaTime;
            if (livePointsTextTimer > 0.5)
                LivePoints.enabled = true;
            if (livePointsTextTimer >= 1)
            {
                LivePoints.enabled = false;
                livePointsTextTimer = 0;
            }
        }
        if (LivesCounter == 1)
        {
            LivePoints.enabled = true;
            LivePoints.color = new Color32(253, 255, 78, 255);
        }
    }

    public void GivePointsPerBlock()
    {
        CurrentScore += pointsPerBlock;
        PointsText.text = CurrentScore.ToString();
    }

    public void MinusLiveOnLoose()
    {
        LivesCounter--;
        LivePoints.text = LivesCounter.ToString();
        if (LivesCounter == 0)
            LivePoints.color = Color.red;
        else if (LivesCounter == -1)
        {
            LivePoints.text = "";          
            FindObjectOfType<SceneLoader>().GameOver();
        }
    }

    public void BonusToScore(int bonus)
    {
        CurrentScore += bonus;
        PointsText.text = CurrentScore.ToString();
    }

    public void PlusToLives()
    {
        LivesCounter++;
        LivePoints.text = LivesCounter.ToString();
    }

    public void ResetGameStats()
    {
        Destroy(gameObject);
    }
}
