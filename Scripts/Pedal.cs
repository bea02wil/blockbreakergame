﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pedal : MonoBehaviour
{
    [SerializeField] AudioClip ballOnPaddleSound;   

    bool dontPlayBallSoundAtStart;
    float screenWidthInUnits;
    float mousePosition;
    float padlePosX;
    float volume = 0.7f;
    Pause gameOn;
    Vector2 padlePos;

    public bool DontPlayBallSoundAtStart
    {
        get { return dontPlayBallSoundAtStart; }
        set { dontPlayBallSoundAtStart = value; }
    }

    private void Start()
    {
        gameOn = FindObjectOfType<Pause>();
    }

    void Update()
    {
        MovePaddle();      
    }

    private void MovePaddle()
    {
        if (gameOn.Paused)
        {
            FreezePedal();
        }

        else
        {
            GetMove();
        }
    }

    private void GetMove()
    {
        screenWidthInUnits = 18.36f;
        mousePosition = Input.mousePosition.x / Screen.width * screenWidthInUnits;
        padlePosX = Mathf.Clamp(mousePosition, 4.123f, 17.2f);
        padlePos = new Vector2(padlePosX, transform.position.y);
        transform.position = padlePos;
    }

    private void FreezePedal()
    {
        padlePos = new Vector2(transform.position.x, transform.position.y);
        transform.position = padlePos;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (DontPlayBallSoundAtStart == true)      
            AudioSource.PlayClipAtPoint(ballOnPaddleSound, Camera.main.transform.position, volume);                     
    }
}
