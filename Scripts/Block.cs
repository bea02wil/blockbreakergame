﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    [SerializeField] AudioClip blockSound;    
    [SerializeField] GameObject playDestroyEffect;
    [SerializeField] ballForLaser laserBall;
    [SerializeField] PlusLive plusLive;
    [SerializeField] BigBall bigBall;
    [SerializeField] Sprite[] spriteHits;

    ballForLaser showLaserBall;
    PlusLive showPlusLive;
    BigBall showBigBall;
    LoseLiveBlock loseLiveBlock;
    SpriteRenderer showNextSpite;
    Ball ball;  
    int dropValueLaser;
    int dropValuePlusLive;
    int dropValueBigBall; 
    int timeHits;
    float volume = 0.6f;

    private void Start()
    {
        ball = FindObjectOfType<Ball>();
        loseLiveBlock = FindObjectOfType<LoseLiveBlock>();
        showNextSpite = GetComponent<SpriteRenderer>();
        FindAndCountBlocks();
    }

    private void FindAndCountBlocks()
    {
        if (tag == "Breakable")
            FindObjectOfType<BlocksCounter>().CountBlocks();
    }

    private void Update()
    {
        BigBallPower();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {      
       AudioSource.PlayClipAtPoint(blockSound, Camera.main.transform.position, volume);
        if (tag == "Breakable")
        {            
            DestroyBlocks();                                                     
        }
    }

    private void DestroyBlocks()
    {
        timeHits++;
        int maxHits = spriteHits.Length + 1;
        if (timeHits == maxHits)
        {
            Destroy(gameObject);
            FindObjectOfType<GameStats>().GivePointsPerBlock();
            FindObjectOfType<BlocksCounter>().CheckCountOfBlocksAndLoadNextLvl();
            PlayEffectOnTrigger();
            DropBonus();
        }            
        else
        {
            ShowNextSprite();
        }
    }

    private void ShowNextSprite()
    {
        int spriteIndex = timeHits - 1;
        if (spriteHits[spriteIndex] != null)
            showNextSpite.sprite = spriteHits[spriteIndex];
        else
        {
            Debug.LogError($"Block sprite is missing from array {gameObject.name}");
        }
    }

    private void PlayEffectOnTrigger()
    {
        GameObject sparkles = Instantiate(playDestroyEffect, transform.position, transform.rotation);
        Destroy(sparkles, 1f);
    }

    private void DropBonus()
    {
        dropValueLaser = Random.Range(1, 16);
        dropValuePlusLive = Random.Range(1, 18);
        dropValueBigBall = Random.Range(1, 18);

        if (dropValueLaser == 10)        
            showLaserBall = Instantiate(laserBall, transform.position, transform.rotation);           
        
        if (dropValuePlusLive == 2 && dropValueLaser != 10)        
            showPlusLive = Instantiate(plusLive, transform.position, transform.rotation);
        if (dropValueBigBall == 5 && dropValuePlusLive != 2 && dropValueLaser != 10
            && FindObjectOfType<GameStats>().CurrentLevel() > 2)
            showBigBall = Instantiate(bigBall, transform.position, transform.rotation);     
    }

    private void BigBallPower()
    {
        if (ball.transform.localScale.x == 1.5f && ball.transform.localScale.y == 1.5f) 
            GetComponent<Collider2D>().isTrigger = true;           
        
        if (ball.transform.localScale.x == 1 && ball.transform.localScale.y == 1)
            GetComponent<Collider2D>().isTrigger = false;  
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ball")
        {
            AudioSource.PlayClipAtPoint(blockSound, Camera.main.transform.position, volume);            
            if (tag == "Breakable")
            {
                FindObjectOfType<GameStats>().BonusToScore(5);
                FindObjectOfType<BlocksCounter>().CheckCountOfBlocksAndLoadNextLvl();
                Destroy(gameObject);
            }
                
            if (tag == "Unbreakable")
            {
                FindObjectOfType<GameStats>().BonusToScore(2);
                Destroy(gameObject);
            }                       
        }           
    }
}
