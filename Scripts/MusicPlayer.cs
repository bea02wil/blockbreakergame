﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
  //  AudioSource music;
    bool musicOn = true;

    private void Start()
    {
      //  music = GetComponent<AudioSource>();
    }

    private void Awake()
    {
        if (FindObjectsOfType(GetType()).Length > 1)
            Destroy(gameObject);
        else
            DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        MuteMusicAndSounds();
    }

    private void MuteMusicAndSounds()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (musicOn)
            {
                AudioListener.volume = 0;
               // music.mute = true;
                musicOn = false;
            }

            else
            {
                AudioListener.volume = 1;
              //  music.mute = false;
                musicOn = true; 
            }
        }
    }
}
