﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    [SerializeField] float yPosLeftAndRightLaser = 0.534f;
    [SerializeField] float xPosDiffLeftLaser = 1.165f;
    [SerializeField] float xPosDiffRightLaser = 1.161f;  
    [SerializeField] float xSpeed = 0;
    [SerializeField] float ySpeed = 8;
     
    Vector2 posLeftLaser;
    Vector2 posRightLaser;  
    Rigidbody2D laser;
    Pedal pedal;     
    bool laserNotOnPaddle;
    float xPosLeftLaserDiffrence;
    float xPosRightLaserDifference;
    public float fireRateTimer;
    float laserTimer;
    bool soundChecker;
    public bool CheckTimer { get; set; }
     
    private void Start()
    {
        pedal = FindObjectOfType<Pedal>();
        xPosLeftLaserDiffrence = xPosDiffLeftLaser;
        xPosRightLaserDifference = xPosDiffRightLaser;
        laser = GetComponent<Rigidbody2D>();      
        
    }

    private void Update()
    {
        LockLasersToPaddle();
        TimeOfLaser();
        ShootLaser();      
    }

    private void TimeOfLaser()
    {
        if (CheckTimer == true)
            fireRateTimer += Time.deltaTime;
        laserTimer += Time.deltaTime;
        if (laserTimer > 6f && transform.position.y == yPosLeftAndRightLaser)
            Destroy(gameObject);

    }

    private void LockLasersToPaddle()
    {
        DefineLasersPositions();
    }

    private void DefineLasersPositions()
    {
        if (laserNotOnPaddle == false)
        {
            if (tag == "Left Laser" || tag == "Left Laser2"
                || tag == "Left Laser3" || tag == "Left Laser4" || tag == "Left Laser5"
                || tag == "Left Laser6" || tag == "Left Laser7")
            {
                posLeftLaser = new Vector2(pedal.transform.position.x - xPosLeftLaserDiffrence,
                    yPosLeftAndRightLaser);
                transform.position = posLeftLaser;
            }

            if (tag == "Right Laser" || tag == "Right Laser2"
                || tag == "Right Laser3" || tag == "Right Laser4" || tag == "Right Laser5"
                || tag == "Right Laser6" || tag == "Right Laser7")
            {
                posRightLaser = new Vector2(pedal.transform.position.x + xPosRightLaserDifference,
                    yPosLeftAndRightLaser);
                transform.position = posRightLaser;
            }
        }
    }

    private void ShootLaser()
    {     
        if (Input.GetMouseButton(0))
        {
            Fire();
        }
    }

    private void Fire()
    {
        if (tag == "Left Laser" || tag == "Right Laser")
        {                     
            if (fireRateTimer < 1)
            {
                laserNotOnPaddle = true;
                laser.velocity = new Vector2(xSpeed, ySpeed);          
                if (soundChecker == false)
                {
                    GetComponent<AudioSource>().Play();
                    soundChecker = true;
                }
            }          
            
        }
        if (tag == "Left Laser2" || tag == "Right Laser2")
        {          
            if (fireRateTimer > 1.1f && fireRateTimer < 1.9f)
            {
                laserNotOnPaddle = true;
                laser.velocity = new Vector2(xSpeed, ySpeed);
                if (soundChecker == false)
                {
                    GetComponent<AudioSource>().Play();
                    soundChecker = true;
                }
            }
        }
        if (tag == "Left Laser3" || tag == "Right Laser3")
        {           
            if (fireRateTimer > 2.1f && fireRateTimer < 2.9f)
            {
                laserNotOnPaddle = true;
                laser.velocity = new Vector2(xSpeed, ySpeed);
                if (soundChecker == false)
                {
                    GetComponent<AudioSource>().Play();
                    soundChecker = true;
                }
            }
        }
        if (tag == "Left Laser4" || tag == "Right Laser4")
        {          
            if (fireRateTimer > 3.1f && fireRateTimer < 3.9f)
            {
                laserNotOnPaddle = true;
                laser.velocity = new Vector2(xSpeed, ySpeed);
                if (soundChecker == false)
                {
                    GetComponent<AudioSource>().Play();
                    soundChecker = true;
                }
            }
        }
        if (tag == "Left Laser5" || tag == "Right Laser5")
        {           
            if (fireRateTimer > 4.1f && fireRateTimer < 4.9f)
            {
                laserNotOnPaddle = true;
                laser.velocity = new Vector2(xSpeed, ySpeed);
                if (soundChecker == false)
                {
                    GetComponent<AudioSource>().Play();
                    soundChecker = true;
                }
            }
        }
        if (tag == "Left Laser6" || tag == "Right Laser6")
        {
            if (fireRateTimer > 5.1f && fireRateTimer < 6)
            {              
                laserNotOnPaddle = true;
                laser.velocity = new Vector2(xSpeed, ySpeed);
                if (soundChecker == false)
                {
                    GetComponent<AudioSource>().Play();
                    soundChecker = true;
                }
            }
        }

        if (tag == "Left Laser7" || tag == "Right Laser7")
        {
            if (fireRateTimer > 7)
            {
                laserNotOnPaddle = true;
                laser.velocity = new Vector2(xSpeed, ySpeed);
                if (soundChecker == false)
                {
                    GetComponent<AudioSource>().Play();
                    soundChecker = true;
                }
            }
        }        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Breakable")       
            AddToScore(collision);
        
        if (collision.tag == "Unbreakable")
            Destroy(gameObject);

        if (collision.tag == "Top Wall")
            Destroy(gameObject);

        if (collision.tag == "YellowBlock" || collision.tag == "GreenBlock"
            || collision.tag == "RedBlock" || collision.tag == "BlueBlock")
            Destroy(gameObject);
    }

    private void AddToScore(Collider2D collision)
    {
        FindObjectOfType<GameStats>().BonusToScore(15);
        Destroy(gameObject);
        Destroy(collision.gameObject);
        FindObjectOfType<BlocksCounter>().CheckCountOfBlocksAndLoadNextLvl();
    }
}
