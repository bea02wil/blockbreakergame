﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    [SerializeField] GameObject pausePanel;
    public bool Paused { get; set; }  
   
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!Paused)          
               PauseTheGame();           
            else            
                Return();            
        }
    }

    private void PauseTheGame()
    {
        Time.timeScale = 0;
        Paused = true;
        pausePanel.SetActive(true);
        AudioListener.volume = 0;
        Cursor.visible = true;
        // music.GetComponent<AudioSource>().mute = true;
    }

    private void Return()
    {
        Time.timeScale = 1;
        Paused = false;
        pausePanel.SetActive(false);
        AudioListener.volume = 1;
        Cursor.visible = false;
        // music.GetComponent<AudioSource>().mute = false;
    }

    public void OnButtonContinueClick()
    {
        Return();
    }

    public void OnButtonQuit()
    {
        Application.Quit();
    }
}
