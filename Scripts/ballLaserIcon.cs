﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballLaserIcon : MonoBehaviour
{
    [SerializeField] GameObject playEffectOnBallIcon;
    [SerializeField] AudioClip timeRemainSound;

    BigBallIcon iconBigBall;
    GameObject sparklesEffect;  
    bool timerChecker;
    float ballLaserIconTimer;
    bool checkIconBigBallObject;

    private void Start()
    {
        iconBigBall = FindObjectOfType<BigBallIcon>();
    }

    private void Update()
    {
        PlayLaserEffect();
        ballLaserIconTimer += Time.deltaTime;
        ChangeIBballPosIfPointsBuzy();
    }

    private void ChangeIBballPosIfPointsBuzy()
    {
        if (checkIconBigBallObject == false && iconBigBall != null)
        {
            if (transform.position == iconBigBall.transform.position)
            {
                iconBigBall.transform.position = new Vector2(iconBigBall.transform.position.x - 0.5f, 6.14f);
                checkIconBigBallObject = true;
            }
        }
    }

    private void PlayLaserEffect()
    {
        sparklesEffect = Instantiate(playEffectOnBallIcon, transform.position, transform.rotation);
        Destroy(sparklesEffect, 0.05f);
        if (ballLaserIconTimer > 5f)
        {
            if (timerChecker == false)
            {
                AudioSource.PlayClipAtPoint(timeRemainSound, Camera.main.transform.position);
                timerChecker = true;
            }               
        }           
        if (ballLaserIconTimer > 6f)
            Destroy(gameObject);        
    }
}
