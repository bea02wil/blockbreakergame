﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigBall : MonoBehaviour
{
    [SerializeField] float xPos;
    [SerializeField] float yPos = -2;
    [SerializeField] BigBallIcon bigBallIcon;
    [SerializeField] AudioClip timeRemainSound;
    [SerializeField] AudioClip ballUpSound;
    [SerializeField] ballLaserIcon ballLaserIcon;  

    Rigidbody2D bigBall;
    Ball ball;
    SpriteRenderer previous;
    SpriteRenderer sprite;
    Color32 color;
    BigBallIcon createBigBallIcon;
    Vector2 bigBallIconPos;
    Vector2 newBallIconPos;
    float bigBallTimer;
    float volume = 0.5f;
    bool timerOn;
    bool timerCheck;
    bool grabOnlyOnce = true;
    
    private void Start()
    {
        StartSettings();
    }

    private void StartSettings()
    {
        bigBall = GetComponent<Rigidbody2D>();
        ball = FindObjectOfType<Ball>();
        previous = ball.BallColor;
        color = previous.color;
        sprite = GetComponent<SpriteRenderer>();
        bigBallIconPos = new Vector2(19.92f, 6.14f);
        newBallIconPos = new Vector2(19.92f, 6.14f);
    }

    private void Update()
    {
        Velocity();
        ReturnTheBallState();    
    }

    private void ReturnTheBallState()
    {
        if (timerOn)        
            bigBallTimer += Time.deltaTime;         
                 
        if (bigBallTimer > 4)
        {
            if (timerCheck == false)
            {              
                AudioSource.PlayClipAtPoint(timeRemainSound, Camera.main.transform.position);
                ball.BallStartSpeed.velocity /= 2;
                ball.transform.localScale = new Vector2(1, 1);
                ball.BallColor.color = color;
                ball.PlayOnBallAlways.SetActive(true);
                timerCheck = true;
            }         
        }
        if (bigBallTimer > 5)        
            Destroy(gameObject);                                
    }

    private void Velocity()
    {
        bigBall.velocity = new Vector2(xPos, yPos);       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {       
        if (collision.tag == "Pedal")
        {
            if (grabOnlyOnce)
            {
                timerOn = true;
                AudioSource.PlayClipAtPoint(ballUpSound, Camera.main.transform.position, volume);
                createBigBallIcon = Instantiate(bigBallIcon, bigBallIconPos, transform.rotation);
                sprite.color = new Color32(194, 196, 199, 1);
                BallSetUp();
                grabOnlyOnce = false;
            }          
        }
        if (collision.tag == "Bottom Trigger")
            Destroy(gameObject, 6f);
    }

    private void BallSetUp()
    {
        ball.BallStartSpeed.velocity *= 2;
        color = ball.BallColor.color;
        ball.transform.localScale = new Vector2(1.5f, 1.5f);
        ball.PlayOnBallAlways.SetActive(false);
        ball.BallColor.color = new Color32(194, 196, 199, 255);      
    }
}
