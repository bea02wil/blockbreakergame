﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomTrigger : MonoBehaviour
{
    [SerializeField] AudioClip onTriggerSound;

    Ball ball;
    Pedal pedal;   

    private void Start()
    {
        ball = FindObjectOfType<Ball>();
        pedal = FindObjectOfType<Pedal>();
        gameObject.SetActive(true);
    } 

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ball")
            ReturnBallAndMinusLive();        
    }

    private void ReturnBallAndMinusLive()
    {
        FindObjectOfType<GameStats>().MinusLiveOnLoose();
        ball.BallNotOnPaddle = false;
        pedal.DontPlayBallSoundAtStart = false;
        ball.CheckLeftButton = true;
        AudioSource.PlayClipAtPoint(onTriggerSound, Camera.main.transform.position);
        ball.LockToPaddle();
    }
}
