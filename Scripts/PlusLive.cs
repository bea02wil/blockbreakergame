﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlusLive : MonoBehaviour
{
    [SerializeField] GameObject playOnPlusLive;
    [SerializeField] float xPos;
    [SerializeField] float yPos = -2;
    [SerializeField] AudioClip plusLiveSound;

    Rigidbody2D plusLive;
    float volume = 0.8f;
   
    private void Start()
    {
        plusLive = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        VelocityAndRotation();
        PlayOnPlusEffect();
    }

    private void VelocityAndRotation()
    {
        plusLive.velocity = new Vector2(xPos, yPos);
        transform.Rotate(0, 1, 0);
    }

    private void PlayOnPlusEffect()
    {
        GameObject PlusLiveEffect = Instantiate(playOnPlusLive, transform.position, transform.rotation);
        Destroy(PlusLiveEffect, 0.03f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Pedal")
        {
            PlusOneLive();
        }

        if (collision.tag == "Bottom Trigger")
            Destroy(gameObject);
    }

    private void PlusOneLive()
    {        
        AudioSource.PlayClipAtPoint(plusLiveSound, Camera.main.transform.position, volume);
        FindObjectOfType<GameStats>().PlusToLives();
        Destroy(gameObject, 0.1f);
    }
}
