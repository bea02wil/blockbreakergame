﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuFader : MonoBehaviour
{
    [SerializeField] Image fadeImage;
    MusicPlayer music;
  
    bool fadeIn;
    bool fadeOut;
    float alphaColor;   
    bool fadeInRulesMenu;
    bool fadeInBackButton;
    bool fadeInBackButtonGameOver;
    public Color fadeInColor;
    public Color fadeOutColor;
  
    private void Start()
    {      
        fadeImage.gameObject.SetActive(true);
        fadeImage.color = new Color(fadeOutColor.r, fadeOutColor.g, fadeOutColor.b, 1f);
        fadeOut = true;
        music = FindObjectOfType<MusicPlayer>();
    }

    private void Update()
    {
        FadeInFadeOut();       
    }

    private void FadeInFadeOut()
    {
        if (fadeIn)
        {
            alphaColor = Mathf.Lerp(fadeImage.color.a, 1, Time.deltaTime * 2f);
            fadeImage.color = new Color(fadeInColor.r, fadeInColor.g, fadeInColor.b, alphaColor);
        }

        if (fadeOut)
        {
            alphaColor = Mathf.Lerp(fadeImage.color.a, 0, Time.deltaTime * 2f);
            fadeImage.color = new Color(fadeOutColor.r, fadeOutColor.g, fadeOutColor.b, alphaColor);
        }

        if (fadeInRulesMenu)
        {
            alphaColor = Mathf.Lerp(fadeImage.color.a, 1, Time.deltaTime * 2f);
            fadeImage.color = new Color(fadeInColor.r, fadeInColor.g, fadeInColor.b, alphaColor);
        }

        if (fadeInBackButton)
        {
            alphaColor = Mathf.Lerp(fadeImage.color.a, 1, Time.deltaTime * 2f);
            fadeImage.color = new Color(fadeInColor.r, fadeInColor.g, fadeInColor.b, alphaColor);
        }

        if (fadeInBackButtonGameOver)
        {
            alphaColor = Mathf.Lerp(fadeImage.color.a, 1, Time.deltaTime * 2f);
            fadeImage.color = new Color(fadeInColor.r, fadeInColor.g, fadeInColor.b, alphaColor);
        }

        if (alphaColor > 0.95 && fadeIn)
        {
            fadeIn = false;
            StartGame();
        }
      
        if (alphaColor > 0.95 && fadeInRulesMenu)
        {
            fadeInRulesMenu = false;
            StartRulesMenu();
        }

        if (alphaColor > 0.95 && fadeInBackButton)
        {
            fadeInBackButton = false;
            LoadStartMenu();
        }

        if (alphaColor > 0.95 && fadeInBackButtonGameOver)
        {
            fadeInBackButtonGameOver = false;
            LoadStartMenuFromGameOver();
        }

        if (alphaColor < 0.05 && fadeOut)
        {
            fadeOut = false;
            fadeImage.gameObject.SetActive(false);
        }  
    }

    private void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    private void StartRulesMenu()
    {
        SceneManager.LoadScene("RulesMenu");
    }

    private void LoadStartMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void ButtonLoadStartMenuFromGameOver()
    {
        fadeImage.gameObject.SetActive(true);
        fadeImage.color = new Color(fadeInColor.r, fadeInColor.g, fadeInColor.b, 0);
        fadeInBackButtonGameOver = true;
    }

    public void ButtonLoadStartMenu()
    {
        fadeImage.gameObject.SetActive(true);
        fadeImage.color = new Color(fadeInColor.r, fadeInColor.g, fadeInColor.b, 0);
        fadeInBackButton = true;
    }

    public void ButtonStartGame()
    {
        fadeImage.gameObject.SetActive(true);
        fadeImage.color = new Color(fadeInColor.r, fadeInColor.g, fadeInColor.b, 0);
        fadeIn = true;
        music.GetComponent<AudioSource>().Play();
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ButtonLoadRules()
    {
        fadeImage.gameObject.SetActive(true);
        fadeImage.color = new Color(fadeInColor.r, fadeInColor.g, fadeInColor.b, 0);
        fadeInRulesMenu = true;
    }

    public void LoadStartMenuFromGameOver()
    {
        FindObjectOfType<GameStats>().ResetGameStats();
        SceneManager.LoadScene(0);
        Cursor.visible = true;
    }
   
}
