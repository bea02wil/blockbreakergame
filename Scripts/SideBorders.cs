﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideBorders : MonoBehaviour
{
    Ball ball;

    void Start()
    {
        ball = FindObjectOfType<Ball>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            if (ball.transform.position.y > 11.6f)
            {
                ball.BallStartSpeed.velocity = new Vector2(0, -7);
            }
        }
    }


}
