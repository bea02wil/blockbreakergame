﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballForLaser : MonoBehaviour
{
    [SerializeField] GameObject playOnFallingBall;
    [SerializeField] float xPos;
    [SerializeField] float yPos = 15f;  
    [SerializeField] Laser[] lasers;
    [SerializeField] ballLaserIcon laserIcon;
    [SerializeField] AudioClip laserBallSound;

    GameObject LaserBallEffect;
    Rigidbody2D ballDirection;    
    Laser createLaser;
    ballLaserIcon createLaserIcon;
    Vector2 laserIconPos;
    
    private void Start()
    {
        ballDirection = GetComponent<Rigidbody2D>();
        laserIconPos = new Vector2(19.92f, 6.14f);      
    }

    private void Update()
    {
        PlayLaserEffect();
        BallVelocity();      
    }

    private void PlayLaserEffect()
    {
        LaserBallEffect = Instantiate(playOnFallingBall, transform.position, transform.rotation);
        Destroy(LaserBallEffect, 0.05f);
    }

    private void BallVelocity()
    {
        ballDirection.velocity = new Vector2(xPos, yPos);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Pedal")
        {                               
             InstantiateLaser();
             Destroy(gameObject, 0.1f);
        }
        if (collision.tag == "Bottom Trigger")
            Destroy(gameObject);
    }

    private void InstantiateLaser()
    {
        for (int i = 0; i < lasers.Length; i++)
        {
            createLaser = Instantiate(lasers[i], lasers[i].transform.position, lasers[i].transform.rotation);
            createLaser.CheckTimer = true;
        }       
        createLaserIcon = Instantiate(laserIcon, laserIconPos, transform.rotation);
        AudioSource.PlayClipAtPoint(laserBallSound, Camera.main.transform.position);                   
    }
}
